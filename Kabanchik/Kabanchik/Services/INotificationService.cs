﻿using Kabanchik.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kabanchik.Services
{
    public interface INotificationService
    {
        /// <summary>
        /// Gets the notifications.
        /// </summary>
        /// <returns></returns>
        List<NotificationModel> GetNotifications();

        /// <summary>
        /// Creates the notification
        /// </summary>
        /// <returns></returns>
       NotificationModel CreateNotification(NotificationModel notification);

        /// <summary>
        /// Deletes all notification asynchronous.
        /// </summary>
        /// <returns></returns>
        bool DeleteAllNotifications();
    }
}