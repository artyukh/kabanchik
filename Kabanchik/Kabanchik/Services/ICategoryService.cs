﻿using Kabanchik.Models;
using System.Threading.Tasks;

namespace Kabanchik.Services
{
    public interface ICategoryService
    {
        /// <summary>
        /// Gets the categories asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<CategoryResponseModel> GetCategoriesAsync();
    }
}