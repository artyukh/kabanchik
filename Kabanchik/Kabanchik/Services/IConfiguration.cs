﻿namespace Kabanchik.Services
{
    public interface IConfiguration
    {
        /// <summary>
        /// Gets or sets the API reference.
        /// </summary>
        /// <value>
        /// The API reference.
        /// </value>
        string ApiReference { get; }
    }
}
