﻿using Kabanchik.Models;
using Prism.Services;
using Refit;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kabanchik.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IPageDialogService _dialogService;
        private readonly IWebApi _webApi;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryService"/> class.
        /// </summary>
        public CategoryService(IConfiguration configuration, IPageDialogService dialogService)
        {
            var httpClient = new HttpClient() { BaseAddress = new Uri(configuration.ApiReference) };
            _webApi = RestService.For<IWebApi>(httpClient);
            _dialogService = dialogService;
        }

        /// <summary>
        /// Gets the categories asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task<CategoryResponseModel> GetCategoriesAsync()
        {
            try
            {
                return await _webApi.GetCategories();
            }
            catch (Exception e)
            {
                await _dialogService.DisplayAlertAsync("Ошибка", e.Message, "Ok");
                return new CategoryResponseModel();
            }  
        }
    }
}
