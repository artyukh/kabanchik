﻿using Kabanchik.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;

namespace Kabanchik.Services
{
    internal class TasksRepository<T> : IRepository<T> where T : TaskModel, new()
    {
        /// <summary>
        /// The database path
        /// </summary>
        private readonly string _dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "tasks.db3");

        /// <summary>
        /// The connection
        /// </summary>
        private readonly SQLiteConnection _connection;

        /// <summary>
        /// Initializes a new instance of the <see cref="TasksRepository{T}"/> class.
        /// </summary>
        public TasksRepository()
        {
            _connection = new SQLiteConnection(_dbPath);
            _connection.CreateTable<TaskModel>();
        }

        /// <summary>
        /// Creates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public T Create(T item)
        {
            item.Id = Guid.NewGuid();
            item.CreateDate = DateTime.Now;
            item.UpdateDate = DateTime.Now;
            _connection.Insert(item);
            return item;
        }

        /// <inheritdoc />
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            var rowCount = _connection.Delete<TaskModel>(id);
            return Convert.ToBoolean(rowCount);
        }

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns></returns>
        public bool DeleteAll()
        {
            return true;
        }

        /// <summary>
        /// Gets all items.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll()
        {
            return _connection.Table<T>().ToList();
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public T GetItem(Guid id)
        {
            return _connection.Get<T>(id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public T Update(T item)
        {
            item.UpdateDate = DateTime.Now;
            _connection.Update(item);
            return item;
        }
    }
}
