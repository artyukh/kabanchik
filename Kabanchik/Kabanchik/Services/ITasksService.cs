﻿using Kabanchik.Models;
using System;
using System.Collections.Generic;

namespace Kabanchik.Services
{
    public interface ITasksService<T> where T : TaskModel
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByCategoryId(int categoryId);

        /// <summary>
        /// Creates the specified task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <returns></returns>
        T Create(T task);

        /// <summary>
        /// Updates the specified note.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <returns></returns>
        T Update(T task);

        /// <summary>
        /// Deletes the specified note.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <returns></returns>
        bool Delete(T task);

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        T GetById(Guid id);
    }
}
