﻿using Kabanchik.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;

namespace Kabanchik.Services
{
    internal class NotificationRepository<T> : IRepository<T> where T : NotificationModel, new()
    {
        /// <summary>
        /// The database path
        /// </summary>
        private readonly string _dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "tasks.db3");

        /// <summary>
        /// The connection
        /// </summary>
        private readonly SQLiteConnection _connection;

        /// <summary>
        /// Initializes a new instance of the <see cref="TasksRepository{T}"/> class.
        /// </summary>
        public NotificationRepository()
        {
            _connection = new SQLiteConnection(_dbPath);
            _connection.CreateTable<T>();
        }

        /// <summary>
        /// Creates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public T Create(T item)
        {
            item.Id = Guid.NewGuid();
            item.CreateDate = DateTime.Now;
            _connection.Insert(item);
            return item;
        }

        /// <inheritdoc />
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            var rowCount = _connection.Delete<T>(id);
            return Convert.ToBoolean(rowCount);
        }

        /// <summary>
        /// Deletes all.
        /// </summary>
        /// <returns></returns>
        public bool DeleteAll()
        {
            var rowCount = _connection.DeleteAll<T>();
            return Convert.ToBoolean(rowCount);
        }

        /// <summary>
        /// Gets all items.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll()
        {
            return _connection.Table<T>().ToList();
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public T GetItem(Guid id)
        {
            return _connection.Get<T>(id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public T Update(T item)
        {
            _connection.Update(item);
            return item;
        }
    }
}
