﻿using Kabanchik.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kabanchik.Services
{
    public class TasksService<T> : ITasksService<T> where T : TaskModel, new()
    {
        /// <summary>
        /// The notes repository
        /// </summary>
        private readonly IRepository<T> _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TasksService{T}"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public TasksService(IRepository<T> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Creates the specified task.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <returns></returns>
        public T Create(T task)
        {
            return _repository.Create(task);
        }

        public bool Delete(T task)
        {
           return _repository.Delete(task.Id);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAllByCategoryId(int categoryId)
        {
            return _repository.GetAll().Where(t => t.CategoryId == categoryId);
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the note by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public T GetById(Guid id)
        {
            return _repository.GetItem(id);
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the note.
        /// </summary>
        /// <param name="note">The note.</param>
        /// <returns></returns>
        public T Update(T task)
        {
            task.UpdateDate = DateTime.Now;
            return _repository.Update(task);
        }
    }
}
