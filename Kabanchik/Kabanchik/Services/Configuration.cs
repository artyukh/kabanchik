﻿namespace Kabanchik.Services
{
    public class Configuration : IConfiguration
    {
        /// <summary>
        /// Gets or sets the API reference.
        /// </summary>
        /// <value>
        /// The API reference.
        /// </value>
        public string ApiReference => "http://200.7.98.16/xamarin_test";
    }
}
