﻿using System.Collections.Generic;
using System.Linq;
using Kabanchik.Models;

namespace Kabanchik.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IRepository<NotificationModel> _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationService"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public NotificationService(IRepository<NotificationModel> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Creates the notification asynchronous.
        /// </summary>
        /// <returns></returns>
        public NotificationModel CreateNotification(NotificationModel notification)
        {
            return _repository.Create(notification);
        }

        /// <summary>
        /// Deletes all notification asynchronous.
        /// </summary>
        /// <returns></returns>
        public bool DeleteAllNotifications()
        {
            return _repository.DeleteAll();
        }

        /// <summary>
        /// Gets the notifications.
        /// </summary>
        /// <returns></returns>
        public List<NotificationModel> GetNotifications()
        {
            return _repository.GetAll().ToList();
        }
    }
}
