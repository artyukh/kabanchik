﻿using Kabanchik.Models;
using Refit;
using System.Threading.Tasks;

namespace Kabanchik.Services
{
    [Headers("Accept: application/json")]
    public interface IWebApi
    {
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <returns></returns>
        [Get("/categories.json")]
        Task<CategoryResponseModel> GetCategories();    
    }
}