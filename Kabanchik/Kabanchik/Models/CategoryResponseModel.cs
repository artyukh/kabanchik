﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Kabanchik.Models
{
    public class CategoryResponseModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryResponseModel"/> class.
        /// </summary>
        public CategoryResponseModel()
        {
            Categories = new List<Category>();
        }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        [JsonProperty(PropertyName = "count")] 
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        /// <value>
        /// The categories.
        /// </value>
        [JsonProperty(PropertyName = "items")]
        public List<Category> Categories { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        [JsonProperty(PropertyName = "result")]
        public int Result { get; set; }
    }
}
