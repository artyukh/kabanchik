﻿using Prism.Mvvm;
using SQLite;
using System;

namespace Kabanchik.Models
{
    [Table("Notifications")]
    public class NotificationModel : BindableBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationModel"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="text">The text.</param>
        public NotificationModel(string title, string text, string notificationTitle)
        {
            Title = title;
            Text = text;
            NotificationTitle = notificationTitle;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationModel"/> class.
        /// </summary>
        public NotificationModel() {}

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [PrimaryKey]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the notification title.
        /// </summary>
        /// <value>
        /// The notification title.
        /// </value>
        public string NotificationTitle { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        /// <value>
        /// The create date.
        /// </value>
        public DateTime CreateDate { get; set; }
    }
}
