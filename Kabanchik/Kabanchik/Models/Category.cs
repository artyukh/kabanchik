﻿using Newtonsoft.Json;

namespace Kabanchik.Models
{
    public class Category
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        [JsonProperty(PropertyName = "icon")] 
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets the icon background.
        /// </summary>
        /// <value>
        /// The icon background.
        /// </value>
        [JsonProperty(PropertyName = "icon_bg")]
        public string IconBackground { get; set; }

        /// <summary>
        /// Gets or sets the icon foreground.
        /// </summary>
        /// <value>
        /// The icon foreground.
        /// </value>
        [JsonProperty(PropertyName = "icon_fg")]
        public string IconForeground { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
    }
}
