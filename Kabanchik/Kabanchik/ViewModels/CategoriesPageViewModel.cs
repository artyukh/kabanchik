﻿using Kabanchik.Models;
using Kabanchik.Services;
using Kabanchik.Views;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;

namespace Kabanchik.ViewModels
{
    public class CategoriesPageViewModel : ViewModelBase
	{
        private INavigationService _navigationService;
        private readonly ICategoryService _categoryService;
        private readonly IConfiguration _configuration;

        private List<Category> _categories;
        public List<Category> Categories
        {
            get => _categories;
            set => SetProperty(ref _categories, value);
        }

        public DelegateCommand<Category> ItemTappedCommand { get; set; }
         
        public DelegateCommand NotificationsCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainPageViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">The navigation service.</param>
        public CategoriesPageViewModel(INavigationService navigationService, ICategoryService categoryService, IConfiguration configuration) : base(navigationService)
        {
            _navigationService = navigationService;
            _categoryService = categoryService;
            _configuration = configuration;
            Title = "Категории";
            ItemTappedCommand = new DelegateCommand<Category>(OnItemTapped);
            NotificationsCommand = new DelegateCommand(OnShowNotifications);
            LoadCategories();
        }

        /// <summary>
        /// Called when [show notifications].
        /// </summary>
        private async void OnShowNotifications()
        {
            await _navigationService.NavigateAsync(nameof(NotificationsPage));
        }

        /// <summary>
        /// Loads the categories.
        /// </summary>
        public async void LoadCategories()
        {
            var id = 1;
            var response = await _categoryService.GetCategoriesAsync();
            foreach (var item in response.Categories)
            {
                item.Icon = $"{_configuration.ApiReference}/{item.Icon}";
                item.Id = id;
                id++;
            }
            Categories = response.Categories;
        }

        /// <summary>
        /// Navigates to detailed page.
        /// </summary>
        private async void OnItemTapped(Category category)
        {
            var navigationParams = new NavigationParameters { { nameof(Category), category } };
            await _navigationService.NavigateAsync(nameof(TasksPage), navigationParams);
        }
    }
}
