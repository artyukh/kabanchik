﻿using Kabanchik.Models;
using Kabanchik.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;

namespace Kabanchik.ViewModels
{
    public class CreateTaskPageViewModel : ViewModelBase
    {
        private readonly ITasksService<TaskModel> _tasksService;
        private readonly INavigationService _navigationService;
        private readonly INotificationService _notificationService;
        private readonly IPageDialogService _dialogService;
        private Category _category;

        /// <summary>
        /// The note
        /// </summary>
        private TaskModel _task;
        public TaskModel Task
        {
            get => _task;
            set => SetProperty(ref _task, value);
        }

        public DelegateCommand SaveCommand { get; set; } 

        public CreateTaskPageViewModel(INavigationService navigationService, ITasksService<TaskModel> tasksService, INotificationService notificationService, IPageDialogService dialogService) : base(navigationService)
        {
            Task = new TaskModel(); 
            Title = "Новое задание";
            _tasksService = tasksService;
            _navigationService = navigationService;
            _notificationService = notificationService;
            _dialogService = dialogService;
            SaveCommand = new DelegateCommand(OnSave);
        }

        /// <summary>
        /// Called when [navigating to].
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            _category = (Category)parameters[nameof(Category)];
            Task.CategoryId = _category.Id;
        }

        /// <summary>
        /// Called when [save].
        /// </summary>
        private async void OnSave()
        {
            if (Task.Title.Trim() == string.Empty || Task.Text.Trim() == string.Empty)
            {
                await _dialogService.DisplayAlertAsync("Kabanchik", "Вы не заполнили все поля", "Ok");
                return;
            }

            _tasksService.Create(Task);
            var notification = new NotificationModel(Task.Title, _category.Title, "Создано новое задание");
            _notificationService.CreateNotification(notification);
            await _dialogService. DisplayAlertAsync("Kabanchik", "Новое задание успешно создано", "Ok");
            await _navigationService.GoBackAsync(new NavigationParameters {{ nameof(Category), _category }});
        }
    }
}
