﻿using System.Collections.Generic;
using Kabanchik.Models;
using Kabanchik.Services;
using Prism.Commands;
using Prism.Navigation;

namespace Kabanchik.ViewModels
{
    public class NotificationsPageViewModel : ViewModelBase
	{
        private readonly INotificationService _notificationService;
        public DelegateCommand DeleteAllCommand { get; set; }

        private List<NotificationModel> _notificatios;
        public List<NotificationModel> Notificatios
        {
            get => _notificatios;
            set => SetProperty(ref _notificatios, value);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsPageViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">The navigation service.</param>
        /// <param name="notificationService">The notification service.</param>
        public NotificationsPageViewModel(INavigationService navigationService, INotificationService notificationService) : base(navigationService)
        {
            Title = "Уведомления";
            _notificationService = notificationService;
            DeleteAllCommand = new DelegateCommand(OnDeleteAllCommand);
            Notificatios = _notificationService.GetNotifications();
        }

        /// <summary>
        /// Called when [delete all command].
        /// </summary>
        private void OnDeleteAllCommand()
        {
            _notificationService.DeleteAllNotifications();
            Notificatios = _notificationService.GetNotifications();
        }
    }
}
