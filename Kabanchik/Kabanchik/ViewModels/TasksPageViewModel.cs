﻿using Kabanchik.Models;
using Kabanchik.Services;
using Kabanchik.Views;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kabanchik.ViewModels
{
    public class TasksPageViewModel : ViewModelBase
    {
        private readonly ITasksService<TaskModel> _tasksService;
        private readonly INavigationService _navigationService;
        private Category _currentCategory;

        public DelegateCommand<TaskModel> ItemTappedCommand { get; set; }

        public DelegateCommand CreateCommand { get; set; }

        private List<TaskModel> _tasksList;
        public List<TaskModel> TasksList
        {
            get => _tasksList;
            set => SetProperty(ref _tasksList, value);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TasksPageViewModel"/> class.
        /// </summary>
        /// <param name="navigationService">The navigation service.</param>
        public TasksPageViewModel(INavigationService navigationService, ITasksService<TaskModel> tasksService) : base(navigationService)
        {
            _tasksService = tasksService;
            _navigationService = navigationService;
            Title = "Задания";
            ItemTappedCommand = new DelegateCommand<TaskModel>(OnItemTapped);
            CreateCommand = new DelegateCommand(OnCreateCommandAsync);
        }

        /// <summary>
        /// Called when [create command].
        /// </summary>
        private async void OnCreateCommandAsync()
        {
            var navigationParams = new NavigationParameters {{ nameof(Category), _currentCategory }};
            await _navigationService.NavigateAsync(nameof(CreateTaskPage), navigationParams);
        }

        /// <summary>
        /// Navigates to detailed page.
        /// </summary>
        private async void OnItemTapped(TaskModel task)
        {
            var navigationParams = new NavigationParameters{{ nameof(TaskModel), task }};
            await _navigationService.NavigateAsync(nameof(CreateTaskPage), navigationParams);
        }

        /// <summary>
        /// Called when [navigated to].
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            _currentCategory = (Category)parameters[nameof(Category)];
            TasksList = _tasksService.GetAllByCategoryId(_currentCategory.Id).ToList();
        }

    }
}
