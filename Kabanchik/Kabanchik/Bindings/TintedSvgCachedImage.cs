﻿using FFImageLoading.Svg.Forms;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Kabanchik.Bindings
{
    public class TintedSvgCachedImage : SvgCachedImage
    {
        public static BindableProperty TintColorProperty = BindableProperty.Create(nameof(TintColor), typeof(string), typeof(TintedSvgCachedImage), "#FFFFFF", propertyChanged: UpdateColor);

        public string TintColor
        {
            get { return (string)GetValue(TintColorProperty); }
            set { SetValue(TintColorProperty, value); }
        }

        /// <summary>
        /// Updates the color.
        /// </summary>
        /// <param name="bindable">The bindable.</param>
        /// <param name="oldColor">The old color.</param>
        /// <param name="newColor">The new color.</param>
        private static void UpdateColor(BindableObject bindable, object oldColor, object newColor)
        {
            var oldcolor = (string)oldColor;
            var newcolor = (string)newColor;

            if (!oldcolor.Equals(newcolor))
            {
                var view = (TintedSvgCachedImage)bindable;
                var transformations = new List<ITransformation>() {
                    new TintTransformation(newcolor) {
                        EnableSolidColor = true
                    }
                };
                view.Transformations = transformations;
            }
        }
    }
}
