﻿using Prism;
using Prism.Ioc;
using Kabanchik.ViewModels;
using Kabanchik.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Kabanchik.Services;
using Prism.Unity;
using Kabanchik.Models;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Kabanchik
{
    public partial class App : PrismApplication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App() : this(null) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        /// <param name="initializer">The initializer.</param>
        public App(IPlatformInitializer initializer) : base(initializer) { }

        /// <summary>
        /// Called when the PrismApplication has completed it's initialization process.
        /// </summary>
        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync("NavigationPage/CategoriesPage");
        }

        /// <summary>
        /// Used to register types with the container that will be used by your application.
        /// </summary>
        /// <param name="containerRegistry"></param>
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            #region Navigation   
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<CategoriesPage, CategoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<TasksPage, TasksPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateTaskPage, CreateTaskPageViewModel>();
            containerRegistry.RegisterForNavigation<NotificationsPage, NotificationsPageViewModel>();
            #endregion

            #region Types   
            containerRegistry.RegisterSingleton<IConfiguration, Configuration>();
            containerRegistry.RegisterSingleton<ICategoryService, CategoryService>();
            containerRegistry.RegisterSingleton<IRepository<TaskModel>, TasksRepository<TaskModel>>();
            containerRegistry.RegisterSingleton<ITasksService<TaskModel>, TasksService<TaskModel>>();
            containerRegistry.RegisterSingleton<IRepository<NotificationModel>, NotificationRepository<NotificationModel>>();
            containerRegistry.RegisterSingleton<INotificationService, NotificationService>();
            #endregion
        }
    }
}
